package vekker2;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import java.awt.Color;

public class Oszlop extends JPanel implements Megjelenit {

	public Color szin;
	public final int pos;
	public final int labelNumb;
	
	public Oszlop(Color szin, int pos, int labelNumb, boolean bordered) {
		this.szin = szin;
		this.pos = pos;
		this.labelNumb= labelNumb;
		this.setBounds((pos-1)*LABELWIDTH, LABELHEIGHT, LABELWIDTH, (LABELHEIGHT*labelNumb));
		this.setBackground(HATTER);
		if (bordered) this.setBorder(BorderFactory.createLineBorder(szin));
		setLayout(null);
		creator();
		setLabelColour(szin);
	}

	public void setLabelColour(Color fg) {
		this.szin=fg;
		for (int i = 0;i<this.getComponentCount();i++) {
			this.getComponent(i).setForeground(this.szin);
		}
	}
	
	public void setState(int adat) {
		for (int i = 0;i<this.getComponentCount();i++) {
			if (adat % 2==1) {
				this.getComponent(i).setVisible(true);
			}else{
				this.getComponent(i).setVisible(false);
			}
			adat=adat>>1;
		}
	}
	
	public void creator() {
		for (int i = 0;i<this.labelNumb;i++) {
			BinDigit comp=new BinDigit(i);	
			this.add(comp);
		}
	}
	
	public Color getColor() {
		return this.szin;
	}
}

