package vekker2;

import java.time.LocalDateTime;
import java.util.Timer;
import java.util.TimerTask;

public class Main {
	
	private static Timer timer = new Timer();
	public static Ablak ablakom= new Ablak();

	public static void main(String[] args) { 
		ablakom.setVisible(true);
		timer.schedule(melo, 3000, 1000);
	}

    public static TimerTask melo=new TimerTask(){
        @Override
        public void run() {
        	ablakom.oszlop1.setState(LocalDateTime.now().getYear()-2000);
        	ablakom.oszlop2.setState(LocalDateTime.now().getMonthValue());
        	ablakom.oszlop3.setState(LocalDateTime.now().getDayOfMonth());
        	ablakom.oszlop4.setState(LocalDateTime.now().getHour());
        	ablakom.oszlop5.setState(LocalDateTime.now().getMinute());
        	ablakom.oszlop6.setState(LocalDateTime.now().getSecond());
        }   
    };

}