package vekker2;

import javax.swing.JLabel;

public class BinDigit extends JLabel implements Megjelenit{
	
	public int binNumb;
	
	/**
	 * @param binNumb 2 megjelenitendo hatvanya.
	 */
	public BinDigit(int number) {
		this.binNumb = number;
		this.setBounds(0,this.binNumb*LABELHEIGHT,LABELWIDTH, LABELHEIGHT);
		this.setText(String.valueOf((int)Math.pow(2,this.binNumb)));
		this.setHorizontalAlignment(CENTER);
		this.setFont(FONTOM);
		this.setName(String.valueOf(this.binNumb));
	}
}
