package vekker2;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Ablak extends JFrame implements Megjelenit{

	private JPanel idoKijelzo;
	private JPanel cimkeTalca;	
	public Oszlop oszlop1 = new Oszlop(Color.magenta,1,6,SZEGELY);
	public Oszlop oszlop2 = new Oszlop(Color.cyan,2,4,SZEGELY);
	public Oszlop oszlop3 = new Oszlop(Color.orange,3,5,SZEGELY);	
	public Oszlop oszlop4 = new Oszlop(Color.red,4,5,SZEGELY);
	public Oszlop oszlop5 = new Oszlop(Color.yellow,5,6,SZEGELY);
	public Oszlop oszlop6 = new Oszlop(Color.green,6,6,SZEGELY);

	public Ablak() {
		setTitle("Date/Time"); //$NON-NLS-1$
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.idoKijelzo = new JPanel();
		this.idoKijelzo.setBackground(HATTER);
		setContentPane(this.idoKijelzo);
		this.idoKijelzo.setLayout(null);
		
		this.idoKijelzo.add(this.oszlop1);
		this.idoKijelzo.add(this.oszlop2);
		this.idoKijelzo.add(this.oszlop3);
		this.idoKijelzo.add(this.oszlop4);
		this.idoKijelzo.add(this.oszlop5);
		this.idoKijelzo.add(this.oszlop6);
				
		this.cimkeTalca = new JPanel();
		this.cimkeTalca.setBackground(HATTER);
		this.cimkeTalca.setLayout(null);
		getContentPane().add(this.cimkeTalca);
		this.cimkeTalca.setVisible(true);

		this.idoKijelzo.setBounds(0, LABELHEIGHT,getFullWidth(),getFullHeight());
		this.setBounds(100, 100,this.idoKijelzo.getWidth()+BORDERWIDTH*2,this.idoKijelzo.getHeight()+BORDERWIDTH+TITLEHEIGHT);
		this.cimkeTalca.setBounds(0,0,this.idoKijelzo.getWidth(),LABELHEIGHT);
		
		Cimke year=new Cimke("Year", this.oszlop1.getColor(), this.oszlop1.getX()); //$NON-NLS-1$
		this.cimkeTalca.add(year);
		Cimke month=new Cimke("Month", this.oszlop2.getColor(), this.oszlop2.getX()); //$NON-NLS-1$
		this.cimkeTalca.add(month);
		Cimke day=new Cimke("Day", this.oszlop3.getColor(), this.oszlop3.getX()); //$NON-NLS-1$
		this.cimkeTalca.add(day);
		Cimke hour=new Cimke("Hour", this.oszlop4.getColor(), this.oszlop4.getX()); //$NON-NLS-1$
		this.cimkeTalca.add(hour);
		Cimke min=new Cimke("Min.", this.oszlop5.getColor(), this.oszlop5.getX()); //$NON-NLS-1$
		this.cimkeTalca.add(min);
		Cimke sec=new Cimke("Sec.", this.oszlop6.getColor(), this.oszlop6.getX()); //$NON-NLS-1$
		this.cimkeTalca.add(sec);


		
	}
	
	public int getFullWidth() {
		int max=0;
		for (int i = 0;i<this.idoKijelzo.getComponentCount();i++) {
			if (this.idoKijelzo.getComponent(i).getX()+this.idoKijelzo.getComponent(i).getWidth()> max){
				max=this.idoKijelzo.getComponent(i).getX()+this.idoKijelzo.getComponent(i).getWidth();
			}
		}
		return max;
	}
	
	public int getFullHeight() {
		int max=0;
		for (int i = 0;i<this.idoKijelzo.getComponentCount();i++) {
			if (this.idoKijelzo.getComponent(i).getY()+this.idoKijelzo.getComponent(i).getHeight()> max){
				max=this.idoKijelzo.getComponent(i).getY()+this.idoKijelzo.getComponent(i).getHeight();
			}
		}
		return max;
	}
}
