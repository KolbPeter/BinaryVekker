package vekker2;

import java.awt.Color;
import java.awt.Font;

public interface Megjelenit {
	
	int MERET=20;
	Font FONTOM=(new Font(Font.DIALOG, Font.BOLD, MERET));
	Font CIMKEFONT=(new Font(FONTOM.getFontName(), FONTOM.getStyle(), FONTOM.getSize()/2));
	int LABELWIDTH = FONTOM.getSize()*2;
	int LABELHEIGHT= LABELWIDTH/2+5;
	int TITLEHEIGHT=29;
	int BORDERWIDTH=4;
	Color HATTER=Color.BLACK;
	boolean SZEGELY = true;
}
