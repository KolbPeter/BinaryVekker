package vekker2;

import java.awt.Color;
import javax.swing.JLabel;

public class Cimke extends JLabel implements Megjelenit{
	
	public int position;
	
	public Cimke(String name,Color szin,int pos) {
		this.position=pos;
		this.setBounds(this.position,0,LABELWIDTH, LABELHEIGHT);
		this.setText(name);
		this.setHorizontalAlignment(CENTER);
		this.setFont(CIMKEFONT);
		this.setBackground(HATTER);
		this.setForeground(szin);
	}
}
